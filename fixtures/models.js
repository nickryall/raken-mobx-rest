const user = {
  "id": "1",
  "type": "User",
  "title": "Mr",
  "firstName": "Nick",
  "lastName": "Ryall",
  "email": "nick.ryall@gmail.com",
  "phone": "021552497"
};

const company = {
  "id": "1",
  "type": "Company",
  "name": "Acme Inc",
  "streetAddress": "98 Gilles Avenue.",
  "city": "Auckland",
  "state": "Auckland",
  "zipCode": "1023",
  "country": "New Zealand",
  "phone": "021552497",
  "subdomain": "acme",
  "primaryColor": "#005493"
};

export { 
  user, 
  company
};