const users = {
  "page": {
    "previous": "http://example.com/users?page=1",
    "self": "http://example.com/users?page=2",
    "next": "http://example.com/users?page=3",
  },
  "data": [
    {
      "id": "1",
      "type": "User",
      "title": "Mr",
      "firstName": "Nick",
      "lastName": "Ryall",
      "email": "nick.ryall@gmail.com",
      "phone": "021552497"
    },
    {
      "id": "2",
      "type": "User",
      "title": "Mr",
      "firstName": "John",
      "lastName": "Jones",
      "email": "john.jones@gmail.com",
      "phone": "021552497"
    }
  ]
};

const companies = {
  "page": {
    "previous": "http://example.com/companies?page=1",
    "self": "http://example.com/companies?page=2",
    "next": "http://example.com/companies?page=3",
  },
  "data": [
    {
      "id": "1",
      "type": "Company",
      "name": "Acme Inc",
      "streetAddress": "98 Gilles Avenue.",
      "city": "Auckland",
      "state": "Auckland",
      "zipCode": "1023",
      "country": "New Zealand",
      "phone": "021552497",
      "subdomain": "acme",
      "primaryColor": "#005493"
    },
    {
      "id": "2",
      "type": "Company",
      "name": "Another Company",
      "streetAddress": "59 Rua Road.",
      "city": "Auckland",
      "state": "Auckland",
      "zipCode": "0602",
      "country": "New Zealand",
      "phone": "021552497",
      "subdomain": "acme",
      "primaryColor": "#005493"
    }
  ]
}
    
export { 
  users, 
  companies, 
};