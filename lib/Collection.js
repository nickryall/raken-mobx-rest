'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _desc, _value, _class, _descriptor, _descriptor2;

var _lodash = require('lodash.camelcase');

var _lodash2 = _interopRequireDefault(_lodash);

var _lodash3 = require('lodash.last');

var _lodash4 = _interopRequireDefault(_lodash3);

var _lodash5 = require('lodash.isempty');

var _lodash6 = _interopRequireDefault(_lodash5);

var _lodash7 = require('lodash.difference');

var _lodash8 = _interopRequireDefault(_lodash7);

var _mobx = require('mobx');

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _Model = require('./Model');

var _Model2 = _interopRequireDefault(_Model);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _initDefineProp(target, property, descriptor, context) {
  if (!descriptor) return;
  Object.defineProperty(target, property, {
    enumerable: descriptor.enumerable,
    configurable: descriptor.configurable,
    writable: descriptor.writable,
    value: descriptor.initializer ? descriptor.initializer.call(context) : void 0
  });
}

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
  var desc = {};
  Object['ke' + 'ys'](descriptor).forEach(function (key) {
    desc[key] = descriptor[key];
  });
  desc.enumerable = !!desc.enumerable;
  desc.configurable = !!desc.configurable;

  if ('value' in desc || desc.initializer) {
    desc.writable = true;
  }

  desc = decorators.slice().reverse().reduce(function (desc, decorator) {
    return decorator(target, property, desc) || desc;
  }, desc);

  if (context && desc.initializer !== void 0) {
    desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
    desc.initializer = undefined;
  }

  if (desc.initializer === void 0) {
    Object['define' + 'Property'](target, property, desc);
    desc = null;
  }

  return desc;
}

function _initializerWarningHelper(descriptor, context) {
  throw new Error('Decorating class property failed. Please ensure that transform-class-properties is enabled.');
}

var Collection = (_class = function () {
  function Collection(data) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    _classCallCheck(this, Collection);

    _initDefineProp(this, 'fetching', _descriptor, this);

    _initDefineProp(this, 'saving', _descriptor2, this);

    this.paging = _mobx.observable.map({});
    this.models = (0, _mobx.observable)([]);

    this.fetching = false;
    this.saving = false;

    if (options.rootStore) {
      this.rootStore = options.rootStore;
    }

    if (!(0, _lodash6.default)(data)) {
      this.set(data);
    }
  }

  /**
   * The collection URL
   */


  _createClass(Collection, [{
    key: 'url',
    value: function url() {
      return '/';
    }

    /**
     * Specifies the model class for that collection
     */

  }, {
    key: 'model',
    value: function model() {
      return _Model2.default;
    }

    /**
     * Gets the unique ids of all the items in the collection
     */

  }, {
    key: 'ids',
    value: function ids() {
      return this.models.map(function (model) {
        return model.uniqueId;
      });
    }

    /**
     * Getter for the collection length
     */

  }, {
    key: 'at',


    /**
     * Get a model at a given position
     */
    value: function at(index) {
      return this.models[index];
    }

    /**
     * Get a model with the given id or uuid
     */

  }, {
    key: 'get',
    value: function get(uniqueId) {
      return this.models.find(function (model) {
        return model.uniqueId === uniqueId;
      });
    }

    /**
     * Controls boolean value of request label
     */

  }, {
    key: 'setRequestLabel',
    value: function setRequestLabel(label) {
      var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      this[label] = state;
    }

    /**
     * Handles full JSON payload and sets data accordingly.
     */

  }, {
    key: 'set',
    value: function set(data, options) {
      // Merge in the any options with the default
      options = Object.assign({
        add: true,
        merge: true,
        remove: true
      }, options);

      if (data.page) {
        this.setPaging(data.page);
      }

      if (data.data) {
        this.setModels(data.data, options);
      }
    }

    /**
     * Sets the links data into the collection.
     */

  }, {
    key: 'setPaging',
    value: function setPaging() {
      var paging = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      this.paging.merge(paging);
    }

    /**
     * Get a link with the given key
     */

  }, {
    key: 'getPagingKey',
    value: function getPagingKey(key) {
      return this.paging.get(key);
    }

    /**
     * Shortcut to the next page
     */

  }, {
    key: 'setModels',
    value: function setModels() {
      var _this = this;

      var models = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
      var options = arguments[1];

      // Merge in the any options with the default
      options = Object.assign({
        add: true,
        merge: true,
        remove: true
      }, options);

      if (options.remove) {
        var ids = models.map(function (d) {
          return d.id;
        });

        this.spliceModels((0, _lodash8.default)(this.ids(), ids));
      }

      models.forEach(function (data) {
        var model = _this.get(data.id);

        if (model && options.merge) model.set(data);

        if (!model && options.add) _this.pushModels(data);
      });
    }

    /**
     * Add a model (or an array of models) to the collection
     */

  }, {
    key: 'add',
    value: function add(models) {
      // Handle single model
      if (!Array.isArray(models)) models = [models];

      this.setModels(models, { add: true, merge: false, remove: false });

      return this.models;
    }

    /**
     * Adds a collection of models.
     * Returns the added models.
     */

  }, {
    key: 'pushModels',
    value: function pushModels(models) {
      var _this2 = this,
          _models;

      // Handle single model
      if (!Array.isArray(models)) models = [models];

      // Get the type of Model
      var CollectionModel = this.model();

      var instances = models.map(function (model) {
        if (model instanceof _Model2.default) {
          if (!(model instanceof CollectionModel)) {
            throw new Error('Collection can only hold ' + CollectionModel.name + ' models.');
          } else {
            if (!model.collection) {
              model.collection = _this2;
            }

            if (!model.rootStore && _this2.rootStore) {
              model.rootStore = _this2.rootStore;
            }

            return model;
          }
        } else {
          return new CollectionModel(model, {
            collection: _this2,
            rootStore: _this2.rootStore
          });
        }
      });

      (_models = this.models).push.apply(_models, _toConsumableArray(instances));

      return instances;
    }

    /**
     * Remove a model (or an array of models) from the collection
     */

  }, {
    key: 'remove',
    value: function remove(models) {
      // Handle single model
      if (!Array.isArray(models)) models = [models];

      var ids = models.map(function (model) {
        return model.uniqueId;
      });

      this.spliceModels(ids);
    }

    /**
     * Removes the models with the given ids or uuids
     */

  }, {
    key: 'spliceModels',
    value: function spliceModels() {
      var _this3 = this;

      var ids = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

      ids.forEach(function (id) {
        var model = _this3.get(id);
        if (!model) return;

        _this3.models.splice(_this3.models.indexOf(model), 1);
      });
    }

    /**
     * Fetches the collection data from the backend.
     *
     * It uses `set` internally so you can
     * use the options to disable adding, changing
     * or removing.
     */

  }, {
    key: 'fetch',
    value: function fetch() {
      var _this4 = this;

      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      // Merge in the any options with the default
      options = Object.assign({
        url: this.url(),
        params: {},
        add: true,
        merge: true,
        remove: true
      }, options);

      this.setRequestLabel('fetching', true);

      return new Promise(function (resolve, reject) {
        // Optionally the request above could also be done as
        _axios2.default.get(options.url, {
          params: options.params
        }).then(function (response) {
          _this4.set(response.data, { add: options.add, merge: options.merge, remove: options.remove });
          _this4.setRequestLabel('fetching', false);
          resolve(_this4, response);
        }).catch(function (error) {
          _this4.setRequestLabel('fetching', false);
          reject(error);
        });
      });
    }

    /**
     * Creates the model and saves it on the backend
     *
     * The default behaviour is optimistic but this
     * can be tuned.
     */

  }, {
    key: 'create',
    value: function create() {
      var _this5 = this;

      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : { wait: false };

      // Don't add/create existing models
      if (data.id && this.get(data.id)) return false;

      var ModelClass = this.model();

      var model = new ModelClass(data);

      return new Promise(function (resolve, reject) {
        if (!options.wait) {
          _this5.add(model);
          resolve(model);
        } else {
          _this5.setRequestLabel('saving', true);
        }

        // Model can create itself
        model.create(null, {
          url: _this5.url()
        }).then(function (model, response) {
          if (options.wait) {
            _this5.add(model);
          }

          _this5.setRequestLabel('saving', false);

          resolve(model, response);
        }).catch(function (error) {
          _this5.setRequestLabel('saving', false);

          // Remove the model if unsuccessful
          _this5.remove(model);

          reject(error);
        });
      });
    }
  }, {
    key: 'length',
    get: function get() {
      return this.models.length;
    }
  }, {
    key: 'nextPage',
    get: function get() {
      return this.getPagingKey('next');
    }

    /**
     * Shortcut to the previous page
     */

  }, {
    key: 'previousPage',
    get: function get() {
      return this.getPagingKey('previous');
    }

    /**
     * Sets the models into the collection.
     *
     * You can disable adding, merging or removing.
     */

  }]);

  return Collection;
}(), (_descriptor = _applyDecoratedDescriptor(_class.prototype, 'fetching', [_mobx.observable], {
  enumerable: true,
  initializer: null
}), _descriptor2 = _applyDecoratedDescriptor(_class.prototype, 'saving', [_mobx.observable], {
  enumerable: true,
  initializer: null
}), _applyDecoratedDescriptor(_class.prototype, 'length', [_mobx.computed], Object.getOwnPropertyDescriptor(_class.prototype, 'length'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'setRequestLabel', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'setRequestLabel'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'set', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'set'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'setPaging', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'setPaging'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'nextPage', [_mobx.computed], Object.getOwnPropertyDescriptor(_class.prototype, 'nextPage'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'previousPage', [_mobx.computed], Object.getOwnPropertyDescriptor(_class.prototype, 'previousPage'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'setModels', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'setModels'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'add', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'add'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'pushModels', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'pushModels'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'remove', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'remove'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'spliceModels', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'spliceModels'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'fetch', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'fetch'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'create', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'create'), _class.prototype)), _class);
exports.default = Collection;