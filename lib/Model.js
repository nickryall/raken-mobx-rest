'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _desc, _value, _class, _descriptor, _descriptor2, _descriptor3;

var _uuidV = require('uuid-v4');

var _uuidV2 = _interopRequireDefault(_uuidV);

var _lodash = require('lodash.isempty');

var _lodash2 = _interopRequireDefault(_lodash);

var _lodash3 = require('lodash.result');

var _lodash4 = _interopRequireDefault(_lodash3);

var _lodash5 = require('lodash.pickby');

var _lodash6 = _interopRequireDefault(_lodash5);

var _lodash7 = require('lodash.pick');

var _lodash8 = _interopRequireDefault(_lodash7);

var _lodash9 = require('lodash.omit');

var _lodash10 = _interopRequireDefault(_lodash9);

var _lodash11 = require('lodash.forin');

var _lodash12 = _interopRequireDefault(_lodash11);

var _mobx = require('mobx');

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _initDefineProp(target, property, descriptor, context) {
  if (!descriptor) return;
  Object.defineProperty(target, property, {
    enumerable: descriptor.enumerable,
    configurable: descriptor.configurable,
    writable: descriptor.writable,
    value: descriptor.initializer ? descriptor.initializer.call(context) : void 0
  });
}

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
  var desc = {};
  Object['ke' + 'ys'](descriptor).forEach(function (key) {
    desc[key] = descriptor[key];
  });
  desc.enumerable = !!desc.enumerable;
  desc.configurable = !!desc.configurable;

  if ('value' in desc || desc.initializer) {
    desc.writable = true;
  }

  desc = decorators.slice().reverse().reduce(function (desc, decorator) {
    return decorator(target, property, desc) || desc;
  }, desc);

  if (context && desc.initializer !== void 0) {
    desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
    desc.initializer = undefined;
  }

  if (desc.initializer === void 0) {
    Object['define' + 'Property'](target, property, desc);
    desc = null;
  }

  return desc;
}

function _initializerWarningHelper(descriptor, context) {
  throw new Error('Decorating class property failed. Please ensure that transform-class-properties is enabled.');
}

// Throw an error when a URL is needed, and none is supplied.
var urlError = function urlError() {
  throw new Error('A url" property or function must be specified');
};

var Model = (_class = function () {
  function Model(data, options) {
    _classCallCheck(this, Model);

    _initDefineProp(this, 'fetching', _descriptor, this);

    _initDefineProp(this, 'saving', _descriptor2, this);

    _initDefineProp(this, 'deleting', _descriptor3, this);

    // Merge in the any options with the default
    options = Object.assign({
      parse: true,
      stripUndefined: true,
      stripNonRest: true
    }, options);

    this.uuid = (0, _uuidV2.default)();
    this.fetching = true;
    this.saving = false;
    this.deleting = false;
    this.attributes = _mobx.observable.map({});
    this.defineProperties();

    if (options.collection) {
      this.collection = options.collection;
    }

    if (options.rootStore) {
      this.rootStore = options.rootStore;
    }

    if (!(0, _lodash2.default)(data)) {
      this.set(data, options);
    }
  }

  _createClass(Model, [{
    key: 'defineProperties',


    /**
     * Create getters/setters for the allowed attributes
     */
    value: function defineProperties() {
      var _this = this;

      this.restAttributes.map(function (key) {
        Object.defineProperty(_this, key, {
          get: function get() {
            return _this.attributes.get(key);
          },
          set: function set(value) {
            _this.attributes.set(key, value);
          },
          enumerable: true,
          configurable: true
        });
      });
    }

    /**
     * The model URL
     */

  }, {
    key: 'url',
    value: function url() {
      // Get the base URL specified as urlRoot or in the collection:
      var base = this.urlRoot || (0, _lodash4.default)(this.collection, 'url') || urlError();

      if (this.isNew) {
        return base;
      }

      return base + (base.charAt(base.length - 1) === '/' ? '' : '/') + encodeURIComponent(this.id);
    }

    /**
     * Returns the unique identifier of the model.
     * Returns either the server id or fall back to client uuid.
     */

  }, {
    key: 'setRequestLabel',
    value: function setRequestLabel(label) {
      var state = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

      this[label] = state;
    }

    /**
     * Sets the attributes data via merge
     */

  }, {
    key: 'set',
    value: function set() {
      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
      var options = arguments[1];

      // Merge in the any options with the default
      options = Object.assign({
        parse: true,
        stripUndefined: true,
        stripNonRest: true
      }, options);

      // Parse the data 
      if (options.parse) {
        data = this.parse(data);
      }

      // Strip out any undefined keys.
      if (options.stripUndefined) {
        data = (0, _lodash6.default)(data, function (prop) {
          return prop !== undefined;
        });
      }

      // Strip out any keys not specificly set as rest attributes.
      if (options.stripNonRest) {
        data = this.stripNonRestAttributes(data);
      }

      this.attributes.merge(data);

      return this;
    }

    /**
     * Strips out any non-recognized attributes when saving to/from
     * the API
     */

  }, {
    key: 'stripNonRestAttributes',
    value: function stripNonRestAttributes(data) {
      var _this2 = this;

      (0, _lodash12.default)(data, function (value, key) {
        if (_this2.restAttributes.indexOf(key) === -1) {
          data = (0, _lodash10.default)(data, key);
        }
      });

      return data;
    }

    /**
     * Converts a response into the hash of attributes to be `set` on the model. 
     * The default implementation is just to pass the response along.
     */

  }, {
    key: 'parse',
    value: function parse(attributes) {
      return attributes;
    }

    /**
     * Clears the models attributes
     */

  }, {
    key: 'clear',
    value: function clear() {
      this.attributes.clear();
    }

    /**
     * Picks properties and returns them as an object.
     */

  }, {
    key: 'pick',
    value: function pick(properties) {
      return (0, _lodash8.default)(this.attributes, properties);
    }

    /**
     * Fetch the model from the server.
     */

  }, {
    key: 'fetch',
    value: function fetch() {
      var _this3 = this;

      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

      this.setRequestLabel('fetching', true);

      var url = options.url ? options.url : this.url();

      return new Promise(function (resolve, reject) {
        _axios2.default.get(url, {
          params: options.params ? options.params : {}
        }).then(function (response) {
          _this3.set(response.data);
          _this3.setRequestLabel('fetching', false);
          resolve(_this3, response);
        }).catch(function (error) {
          _this3.setRequestLabel('fetching', false);
          reject(error);
        });
      });
    }

    /**
     * Save the model to the server via a PATCH request.
     * If the model is new delegates to the create action.
     * If the `wait` option is false it will optimistically 
     * update the data passed
     */

  }, {
    key: 'save',
    value: function save() {
      var _this4 = this;

      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var options = arguments[1];

      // Merge in the any options with the default
      options = Object.assign({
        wait: false,
        stripNonRest: true
      }, options);

      // Save reference to the current atributes
      var originalAttributes = this.attributes.toJS();

      // Strip out attributes not defined in the restAttributes map
      if (options.stripNonRest) {
        data = this.stripNonRestAttributes(data);
      }

      // If the model does not have an ID. Send a POST request
      if (this.isNew) {
        return this.create(data, options);
      }

      if (data === null) {
        data = Object.assign({}, originalAttributes);
      }

      if (options.wait) {
        this.setRequestLabel('saving', true);
      } else {
        this.set(data);
      }

      return new Promise(function (resolve, reject) {
        _axios2.default.patch(_this4.url(), data).then(function (response) {
          _this4.set(response.data);
          _this4.setRequestLabel('saving', false);
          resolve(_this4, response.data);
        }).catch(function (error) {
          if (!options.wait) {
            _this4.set(originalAttributes);
          }

          _this4.setRequestLabel('saving', false);
          reject(error);
        });
      });
    }

    /**
     * Create a new model to the server with  a POST request.
     * If the `wait` option is false it will optimistically 
     * update the attributes and relationships passed in.
     */

  }, {
    key: 'create',
    value: function create() {
      var _this5 = this;

      var data = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : { wait: false };

      var originalAttributes = this.attributes.toJS();

      if (data) {
        data = Object.assign({}, originalAttributes, data);
      } else {
        data = Object.assign({}, originalAttributes);
      }

      if (!options.wait) {
        this.set(data);
      } else {
        this.setRequestLabel('saving', true);
      }

      return new Promise(function (resolve, reject) {
        _axios2.default.post(options.url ? options.url : _this5.url(), data).then(function (response) {
          _this5.set(response.data);
          _this5.setRequestLabel('saving', false);
          resolve(_this5, response);
        }).catch(function (error) {
          if (!options.wait) {
            _this5.set(originalAttributes);
          }

          _this5.setRequestLabel('saving', false);
          reject(error);
        });
      });
    }

    /**
     * Destroy this model on the server if it was already persisted.
     * Optimistically removes the model from its collection, if it has one.
     * If `wait: true` is passed, waits for the server to respond before removal.
     */

  }, {
    key: 'destroy',
    value: function destroy() {
      var _this6 = this;

      var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { wait: false };

      if (this.isNew && this.collection) {
        this.collection.remove(this);

        return true;
      }

      if (!options.wait && this.collection) {
        this.collection.remove(this);
      } else {
        this.setRequestLabel('deleting', true);
      }

      return new Promise(function (resolve, reject) {
        _axios2.default.delete(_this6.url()).then(function (response) {
          if (options.wait && _this6.collection) {
            _this6.collection.remove(_this6);
          }

          _this6.setRequestLabel('deleting', false);

          resolve(_this6, response);
        }).catch(function (error) {
          // Put it back if delete request fails
          if (!options.wait && _this6.collection) {
            _this6.collection.add(_this6);
          }

          _this6.setRequestLabel('deleting', false);

          reject(error);
        });
      });
    }
  }, {
    key: 'restAttributes',
    get: function get() {
      return ['id'];
    }
  }, {
    key: 'uniqueId',
    get: function get() {
      return this.id ? this.id : this.uuid;
    }

    /**
     * Getter to check if a model is yet to be saved to the server
     */

  }, {
    key: 'isNew',
    get: function get() {
      return this.id == null;
    }

    /**
     * Controls boolean value of request label
     */

  }]);

  return Model;
}(), (_descriptor = _applyDecoratedDescriptor(_class.prototype, 'fetching', [_mobx.observable], {
  enumerable: true,
  initializer: null
}), _descriptor2 = _applyDecoratedDescriptor(_class.prototype, 'saving', [_mobx.observable], {
  enumerable: true,
  initializer: null
}), _descriptor3 = _applyDecoratedDescriptor(_class.prototype, 'deleting', [_mobx.observable], {
  enumerable: true,
  initializer: null
}), _applyDecoratedDescriptor(_class.prototype, 'setRequestLabel', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'setRequestLabel'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'set', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'set'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'stripNonRestAttributes', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'stripNonRestAttributes'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'parse', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'parse'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'clear', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'clear'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'pick', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'pick'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'fetch', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'fetch'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'save', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'save'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'create', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'create'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'destroy', [_mobx.action], Object.getOwnPropertyDescriptor(_class.prototype, 'destroy'), _class.prototype)), _class);
exports.default = Model;