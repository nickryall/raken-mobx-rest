import request from 'axios';
import Collection from '../src/Collection';
import Model from '../src/Model';

import { users, companies } from '../fixtures/collections';

describe('Collection', function() {
  describe('constructor with no initial state', function() {
    beforeEach(function() {
      spyOn(Collection.prototype, 'set');

      this.rootStore = {
        name: 'My App'
      };
      this.collection = new Collection([], {
        rootStore: this.rootStore
      });
    });

    it ('Creates a paging map', function() {
      expect(this.collection.paging).toBeDefined();
    });

    it ('Creates a models observable array', function() {
      expect(this.collection.models).toBeDefined();
    });

    it ('Sets the request labels to falsey by default', function() {
      expect(this.collection.fetching).toBeFalsy();
      expect(this.collection.creating).toBeFalsy();
    });

    it ('Does not call set method as there is no initial state', function() {
      expect(this.collection.set).not.toHaveBeenCalled();
    });

    it ('Creates a reference to the rootStore', function() {
      expect(this.collection.rootStore.name).toBe('My App');
    });
  });

  describe('constructor with initial state', function() {
    beforeAll(function() {
      spyOn(Collection.prototype, 'set');
    });

    beforeEach(function() {
      this.collection = new Collection(users);
    });

    it ('Calls set method with the initial state', function() {
      expect(this.collection.set).toHaveBeenCalledWith(users);
    });
  });

  describe('url method', function() {
    it('Create the URL for the collection', function() {
      class subCollection extends Collection {
        url() {
          return `jsonapi/users`;
        };
      };

      this.collection = new subCollection();

      expect(this.collection.url()).toEqual('jsonapi/users');
    });
  });

  describe('model method', function() {
    it('returns the model class by default', function() {
      this.collection = new Collection();

      expect(this.collection.model()).toEqual(Model);
    });

    it('can be overridden to return a different model class', function() {
      class SubModel extends Model {};

      class subCollection extends Collection {
        model() {
          return SubModel;
        }
      }

      this.collection = new subCollection();

      expect(this.collection.model()).toEqual(SubModel);
    });
  });

  describe('"length" getter', function() {
    beforeEach(function() {
      this.collection = new Collection(users);
    });

    it('returns the length of collection.models', function() {
      expect(this.collection.length).toEqual(2);
    });
  });

  describe('ids method', function() {
    beforeEach(function() {
      this.collection = new Collection(users);
    });

    it('returns the unique ids for every model in the collection', function() {
      expect(this.collection.ids()).toEqual(['1', '2']);
    });
  });

  describe('get method', function() {
    beforeEach(function() {
      class UserModel extends Model {
        get restAttributes() {
          return [
            'id',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };
      }

      class UserCollection extends Collection {
        model() {
          return UserModel;
        }
      }

      this.collection = new UserCollection(users);
    });

    it('returns the the model with the given unique id', function() {
      const model = this.collection.get('2');
      expect(model.firstName).toEqual('John');
    });
  });

  describe('at method', function() {
    beforeEach(function() {
      class UserModel extends Model {
        get restAttributes() {
          return [
            'id',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };
      }

      class UserCollection extends Collection {
        model() {
          return UserModel;
        }
      }

      this.collection = new UserCollection(users);
    });

    it('returns the the model at the give index', function() {
      const model = this.collection.at(0);
      expect(model.firstName).toEqual('Nick');
    });
  });

  describe('nextPage getter', function() {
    beforeEach(function() {
      this.collection = new Collection(users);
    });

    it('returns the link item with the given key', function() {
      expect(this.collection.nextPage).toEqual('http://example.com/users?page=3');
    });
  });

  describe('previousPage getter', function() {
    beforeEach(function() {
      this.collection = new Collection(users);
    });

    it('returns the link item with the given key', function() {
      expect(this.collection.previousPage).toEqual('http://example.com/users?page=1');
    });
  });

  describe('setRequestLabel action', function() {
    beforeEach(function() {
      this.collection = new Collection();
    });

    it('Sets the give prop to the boolean value', function() {
      this.collection.setRequestLabel('creating', true);
      expect(this.collection.creating).toBeTruthy();
      this.collection.setRequestLabel('creating', false);
      expect(this.collection.creating).toBeFalsy();
    });
  });

  describe('set action', function() {
    beforeEach(function() {
      spyOn(Collection.prototype, 'setPaging');
      spyOn(Collection.prototype, 'setModels');
    });

    it('Calls the correct set method for each data type', function() {
      this.collection = new Collection(users);

      expect(this.collection.setPaging).toHaveBeenCalledWith(users.page);
      expect(this.collection.setModels).toHaveBeenCalledWith(users.data, { add: true, merge: true, remove: true });
    });
  });

  describe('setModels action with default options', function() {
    beforeEach(function() {
      class UserModel extends Model {
        get restAttributes() {
          return [
            'id',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };
      }

      class UserCollection extends Collection {
        model() {
          return UserModel;
        }
      }

      this.collection = new UserCollection();

      this.collection.setModels([
        {
          "id": "3",
          "type": "User",
          "title": "Mr",
          "first_Name": "James",
          "lastName": "Thomas",
          "email": "james.thomas@gmail.com",
          "phone": "021552497"
        },
        {
          "id": "2",
          "type": "User",
          "title": "Master",
          "firstName": "John",
          "lastName": "Jones",
          "email": "john.jones@gmail.com",
          "phone": "021552497"
        }
      ]);
    });

    it('Should add new models passed in', function() {
      expect(this.collection.get('3')).toBeDefined();
    });

    it('Should remove existing models that are not passed in', function() {
      expect(this.collection.get('1')).not.toBeDefined();
    });

    it('Should update existing models that are passed in', function() {
      expect(this.collection.get('2').title).toEqual('Master');
    });
  });

  describe('setModels action with "add" option set to falsy', function() {
    beforeEach(function() {
      this.collection = new Collection(users);

      this.collection.setModels([
        {
          "id": "3",
          "type": "User",
          "title": "Mr",
          "first_Name": "James",
          "lastName": "Thomas",
          "email": "james.thomas@gmail.com",
          "phone": "021552497"
        },
        {
          "id": "2",
          "type": "User",
          "title": "Master",
          "firstName": "John",
          "lastName": "Jones",
          "email": "john.jones@gmail.com",
          "phone": "021552497"
        }
      ], { add: false, merge: true, remove: true });
    });

    it('Should not add new models to the collection', function() {
      expect(this.collection.length).toEqual(1);
    });
  });

  describe('setModels action with "merge" option set to falsy', function() {
    beforeEach(function() {
      class UserModel extends Model {
        get restAttributes() {
          return [
            'id',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };
      }

      class UserCollection extends Collection {
        model() {
          return UserModel;
        }
      }

      this.collection = new UserCollection(users);

      this.collection.setModels([
        {
          "id": "3",
          "type": "User",
          "title": "Mr",
          "first_Name": "James",
          "lastName": "Thomas",
          "email": "james.thomas@gmail.com",
          "phone": "021552497"
        },
        {
          "id": "2",
          "type": "User",
          "title": "Master",
          "firstName": "John",
          "lastName": "Jones",
          "email": "john.jones@gmail.com",
          "phone": "021552497"
        }
      ], { add: true, merge: false, remove: true });
    });

    it('Should not update existing models', function() {
      expect(this.collection.get('2').title).toEqual('Mr');
    });
  });

  describe('setModels action with "remove" options set to falsy', function() {
    beforeEach(function() {
      class UserModel extends Model {
        get restAttributes() {
          return [
            'id',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };
      }

      class UserCollection extends Collection {
        model() {
          return UserModel;
        }
      }

      this.collection = new UserCollection(users);

      this.collection.setModels([
        {
          "id": "3",
          "type": "User",
          "title": "Mr",
          "first_Name": "James",
          "lastName": "Thomas",
          "email": "james.thomas@gmail.com",
          "phone": "021552497"
        },
        {
          "id": "2",
          "type": "User",
          "title": "Master",
          "firstName": "John",
          "lastName": "Jones",
          "email": "john.jones@gmail.com",
          "phone": "021552497"
        }
      ], { add: true, merge: true, remove: false });
    });

    it('Should not remove models from the collection', function() {
      expect(this.collection.length).toEqual(3);
    });
  });

  describe('add action', function() {
    beforeEach(function() {
      spyOn(Collection.prototype, 'setModels').and.callThrough();

      class UserModel extends Model {
        get restAttributes() {
          return [
            'id',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };
      }

      class UserCollection extends Collection {
        model() {
          return UserModel;
        }
      }

      this.collection = new UserCollection();
    });

    it('Calls set function with the passed in model(s) with only the "add" option set to truthy', function() {
      const modelJSON = {
        "id": "2",
        "type": "User",
        "title": "Mr",
        "firstName": "Tim",
        "lastName": "Smith",
        "email": "tim.smith@gmail.com",
        "phone": "021552497"
      };

      this.collection.add(modelJSON);

      expect(this.collection.setModels).toHaveBeenCalledWith([modelJSON], { add: true, remove: false, merge: false });

      expect(this.collection.at(0).firstName).toEqual('Tim');
    });

    it('Can receive a single JSON representation of a model', function() {
      this.collection.add({
        "id": "1",
        "type": "User",
        "title": "Mr",
        "firstName": "Nick",
        "lastName": "Ryall",
        "email": "nick.ryall@gmail.com",
        "phone": "021552497"
      });

      expect(this.collection.get('1').email).toEqual('nick.ryall@gmail.com');
    });

    it('Can receive a single model instance', function() {
      class UserModel extends Model {
        get restAttributes() {
          return [
            'id',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };
      }

      class UserCollection extends Collection {
        model() {
          return UserModel;
        }
      }

      this.collection = new UserCollection();

      const newModel = new UserModel({
        "id": "2",
        "type": "users",
        "title": "Mr",
        "firstName": "John",
        "lastName": "Jones",
        "email": "john.jones@gmail.com",
        "phone": "021552497"
      });

      this.collection.add(newModel);

      expect(this.collection.get('2').email).toEqual('john.jones@gmail.com');
    });

    it('Can receive an array of JSON representations', function() {
      this.collection.add(users.data);

      expect(this.collection.get('1').email).toEqual('nick.ryall@gmail.com');
      expect(this.collection.get('2').email).toEqual('john.jones@gmail.com');
    });

    it('Can receive an array of model instances', function() {
      class UserModel extends Model {
        get restAttributes() {
          return [
            'id',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };
      }

      class UserCollection extends Collection {
        model() {
          return UserModel;
        }
      }

      this.collection = new UserCollection();


      const newModel1 = new UserModel(users.data[0]);

      const newModel2 = new UserModel(users.data[1]);

      this.collection.add([
        newModel1,
        newModel2
      ]);

      expect(this.collection.get('1').email).toEqual('nick.ryall@gmail.com');
      expect(this.collection.get('2').email).toEqual('john.jones@gmail.com');
    });
  });

  describe('pushModels action', function() {
    it('Instantiates models from JSON', function() {
      class UserModel extends Model {
        get restAttributes() {
          return [
            'id',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };
      }

      class UserCollection extends Collection{
        model() {
          return UserModel;
        }
      };

      this.collection = new UserCollection();

      this.collection.pushModels({
        "id": "1",
        "title": "Mr",
        "firstName": "Nick",
        "lastName": "Ryall",
        "email": "nick.ryall@gmail.com",
        "phone": "021552497"
      });

      expect(this.collection.get('1').email).toEqual('nick.ryall@gmail.com');
    });

    it('Accepts model instances', function() {
      class UserModel extends Model {
        get restAttributes() {
          return [
            'id',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };
      }

      class UserCollection extends Collection{
        model() {
          return UserModel;
        }
      };

      this.collection = new UserCollection();

      const newModel1 = new UserModel(users.data[0]);

      const newModel2 = new UserModel(users.data[1]);

      this.collection.pushModels([
        newModel1,
        newModel2
      ]);

      expect(this.collection.get('1').email).toEqual('nick.ryall@gmail.com');
      expect(this.collection.get('2').email).toEqual('john.jones@gmail.com');
    });

    it('Rejects model instances that are not dervied from the correct class', function() {
      class UserModel extends Model {
        get restAttributes() {
          return [
            'id',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };
      }

      class UserCollection extends Collection{
        model() {
          return UserModel;
        }
      };

      this.collection = new UserCollection();

      const model = new Model();

      expect(() => {
        this.collection.pushModels(model);
      }).toThrow(new Error('Collection can only hold UserModel models.'));

    });

    it('Sets a reference to the collection on the child models', function() {
      class UserModel extends Model {
        get restAttributes() {
          return [
            'id',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };
      }

      class UserCollection extends Collection{
        model() {
          return UserModel;
        }
      };

      this.collection = new UserCollection();

      const newModel1 = new UserModel(users.data[0]);

      this.collection.pushModels([newModel1])

      expect(this.collection.at(0).collection).toEqual(this.collection);
    })

    it('Sets reference to the rootStore on the child models', function() {
      class UserModel extends Model {
        get restAttributes() {
          return [
            'id',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };
      }

      class UserCollection extends Collection{
        model() {
          return UserModel;
        }
      };

      this.rootStore = {};

      this.collection = new UserCollection(null, {
        rootStore: this.rootStore
      });

      const newModel1 = new UserModel(users.data[0]);

      this.collection.pushModels([newModel1])

      expect(this.collection.at(0).rootStore).toEqual(this.rootStore);
    })
  });

  describe('remove action', function() {
    beforeEach(function() {
      spyOn(Collection.prototype, 'spliceModels').and.callThrough();
      this.collection = new Collection(users);
    });

    it('Finds the unique ids of the models(s) and passes them to the spliceModels action', function() {
      this.collection.remove([
        this.collection.at(0)
      ]);

      expect(this.collection.spliceModels).toHaveBeenCalledWith(['1']);
      expect(this.collection.length).toEqual(1);

      const newModel = new Model();

      // Add a new model with no server Id.
      this.collection.add(newModel);

      this.collection.remove([
        this.collection.at(1)
      ]);

      expect(this.collection.spliceModels).toHaveBeenCalledWith([newModel.uniqueId]);
      expect(this.collection.length).toEqual(1);
    });
  });

  describe('spliceModels action', function() {
    beforeEach(function() {
      this.collection = new Collection(users);
    });

    it('Removes the models with the given ids (or uuid)', function() {
      const newModel = new Model();

      // Add a new model with no server Id.
      this.collection.add(newModel);

      expect(this.collection.length).toEqual(3);

      this.collection.spliceModels(['2']);

      expect(this.collection.length).toEqual(2);

      this.collection.spliceModels([newModel.uniqueId]);

      expect(this.collection.length).toEqual(1);
    });
  });

  describe('fetch action', function() {
    beforeEach(function() {
      class SubCollection extends Collection {
        url() {
          return '/api/v1/businesses';
        }
      };

      this.collection = new SubCollection();
    });

    it('Sets the "fetching" request label to truthy', function() {
      this.collection.fetch().then(() => {

      }).catch(() => {

      });

      expect(this.collection.fetching).toBeTruthy();
    });

    it('Calls a get request with the collections url by default', function() {
      spyOn(request, 'get');

      this.collection.fetch().then(() => {

      }).catch(() => {
        
      });

      expect(request.get).toHaveBeenCalledWith(this.collection.url(), { params: {} });
    });

    it('Calls a get request with the url passed in though options', function() {
      spyOn(request, 'get');

      this.collection.fetch({
        url: '/api/v1/users/1/businesses'
      }).then(() => {

      }).catch(() => {
        
      });

      expect(request.get).toHaveBeenCalledWith('/api/v1/users/1/businesses', { params: {} });
    });

    it('Sends any "params" included in the options argument', function() {
      spyOn(request, 'get');

      this.collection.fetch({
        params: {
          included: 'projects'
        }
      }).then(() => {

      }).catch(() => {
        
      });;

      expect(request.get).toHaveBeenCalledWith(this.collection.url(), { params: { included: 'projects'} });
    });

    it('Calls the set action if the request is successful', function() {
      spyOn(Collection.prototype, 'set');

      spyOn(request, 'get').and.callFake(function(url) {
        return {
          then: function(cb) {
            cb.call(null, {
              status: 200,
              data: companies
            });

            return this;
          },
          catch: ()=> {}
         }
      });


      this.collection.fetch().then(() => {

      }).catch(() => {
        
      });;

      expect(this.collection.set).toHaveBeenCalledWith(companies, { add: true, merge: true, remove: true });
      expect(this.collection.fetching).toBeFalsy();
    });

    it('Passes the set options through to the set action', function() {
      spyOn(Collection.prototype, 'set');

      spyOn(request, 'get').and.callFake(function(url) {
        return {
          then: function(cb) {
            cb.call(null, {
              status: 200,
              data: companies
            });

            return this;
          },
          catch: ()=> {}
         }
      });


      this.collection.fetch({ add: true, merge: false, remove: false }).then(() => {

      }).catch(() => {
        
      });;

      expect(this.collection.set).toHaveBeenCalledWith(companies, { add: true, merge: false, remove: false });
    });
    
    it('allows for the individual set options to be overriden', function() {
      spyOn(Collection.prototype, 'set');

      spyOn(request, 'get').and.callFake(function(url) {
        return {
          then: function(cb) {
            cb.call(null, {
              status: 200,
              data: companies
            });

            return this;
          },
          catch: ()=> {}
         }
      });


      this.collection.fetch({ remove: false }).then(() => {

      }).catch(() => {
        
      });;

      expect(this.collection.set).toHaveBeenCalledWith(companies, { add: true, merge: true, remove: false });
    });

    it('Sets the "fetching" request label to falsy if the request fails', function() {
      spyOn(Collection.prototype, 'set');

      spyOn(request, 'get').and.callFake(function(url) {
        return {
          then: function(cb) {
            return this;
          },
          catch: function(cb) {
            cb.call(null, {
              status: 500
            });

            return this;
          }
         }
      });

      this.collection.fetch().then(() => {

      }).catch(() => {
        
      });

      expect(this.collection.set).not.toHaveBeenCalled();
      expect(this.collection.fetching).toBeFalsy();
    });
  });

  describe('create action', function() {
    it('Exits if called with and model id that already exists in collection', function() {
      this.collection = new Collection();

      this.collection.add(users.data);

      // No op as model with id exists
      this.collection.create({
        "id": "1",
        "type": "User",
        "title": "Mr",
        "firstName": "Nick",
        "lastName": "Ryall",
        "email": "nick.ryall@gmail.com",
        "phone": "021552497"
      });

      expect(this.collection.length).toEqual(2);
    });

    it('Creates a new model instance with the passed in data, returns instance once promise is resolved.', function() {
      this.collection = new Collection();

      spyOn(request, 'post').and.callFake(function(url) {
        return {
          then: function(cb) {
            cb.call(null, {
              status: 201
            });
           
            return this;
          },
          catch: ()=> {}
        }
      });

      // No op as model with id exists
      this.collection.create({
        "type": "User",
        "title": "Mr",
        "firstName": "Nick",
        "lastName": "Ryall",
        "email": "nick.ryall@gmail.com",
        "phone": "021552497"
      }).then((model) => {
        expect(this.collection.length).toBe(1);
        expect(model instanceof Model).toBeTruthy(); 
      })
    });

    it('Adds the new model to the collection immediately if "wait" options is falsy', function() {
      spyOn(Model.prototype, 'create').and.callFake(function(url) {
        return {
          then: function(cb) {
            setTimeout(() => {
              cb.call(null, {
                status: 201
              });
            }, 1000);

            return this;
          },
          catch: ()=> {}
        }
      });

      this.collection = new Collection();

      this.collection.create({
        "type": "User",
        "title": "Mr",
        "firstName": "Nick",
        "lastName": "Ryall",
        "email": "nick.ryall@gmail.com",
        "phone": "021552497"
      }, { wait: false }).then(() => {

      }).catch(() => {
        
      });

      expect(this.collection.length).toEqual(1);
    });

    it('Adds the new model only after successful creation on server if "wait" options is truthy', function() {
      spyOn(Model.prototype, 'create').and.callFake(function(url) {
        return {
          then: function(cb) {
            setTimeout(() => {
              cb.call(null, {
                status: 201
              });
            }, 1000);

            return this;
          },
          catch: ()=> {}
        }
      });

      this.collection = new Collection();

      this.collection.create({
        "type": "User",
        "title": "Mr",
        "firstName": "Nick",
        "lastName": "Ryall",
        "email": "nick.ryall@gmail.com",
        "phone": "021552497"
      }, { wait: true });

      expect(this.collection.length).toEqual(0);

      // Fast-forward until all timers have been executed
      jest.runOnlyPendingTimers();

      expect(this.collection.length).toEqual(1);
    });

    it('Sets the "creating" label to truthy if "wait" option is truthy', function() {
      this.collection = new Collection();

      this.collection.create({
        "type": "User",
        "title": "Mr",
        "firstName": "Nick",
        "lastName": "Ryall",
        "email": "nick.ryall@gmail.com",
        "phone": "021552497"
      }, { wait: true }).then(() => {

      }).catch(() => {
        
      });

      expect(this.collection.saving).toBeTruthy();
    });

    it('Calls the create action on the model with the collections URL', function() {
      spyOn(Model.prototype, 'create').and.callFake(function() {
        return {
          then: function() {
            return this;
          }
        }
      });

      this.collection = new Collection();

      this.collection.create({
        "type": "User",
        "title": "Mr",
        "firstName": "Nick",
        "lastName": "Ryall",
        "email": "nick.ryall@gmail.com",
        "phone": "021552497"
      });

      expect(Model.prototype.create.calls.mostRecent().args[1].url).toEqual(this.collection.url());
    });

    it('Sets the "creating" label to falsy after the model.create method completes successfuly', function() {
      spyOn(Model.prototype, 'create').and.callFake(function(url) {
        return {
          then: function(cb) {
            setTimeout(() => {
              cb.call(null, {
                status: 201
              });
            }, 1000);

            return this;
          },
          catch: ()=> {}
        }
      });

      this.collection = new Collection();

      this.collection.create({
        "type": "User",
        "title": "Mr",
        "firstName": "Nick",
        "lastName": "Ryall",
        "email": "nick.ryall@gmail.com",
        "phone": "021552497"
      }, { wait: true }).then(() => {

      }).catch(() => {
        
      });

      expect(this.collection.saving).toBeTruthy();

      // Fast-forward until all timers have been executed
      jest.runOnlyPendingTimers();

      expect(this.collection.saving).toBeFalsy();
    });

    it('Sets the "creating" label to falsy after the model.create method fails', function() {
      spyOn(Model.prototype, 'create').and.callFake(function(url) {
        return {
          then: function(cb) {
            return this;
          },
          catch: function(cb) {
            setTimeout(() => {
              cb.call(null, {
                status: 500
              });
            });

            return this;
          }
         }
      });

      this.collection = new Collection();

      this.collection.create({
        "type": "User",
        "title": "Mr",
        "firstName": "Nick",
        "lastName": "Ryall",
        "email": "nick.ryall@gmail.com",
        "phone": "021552497"
      }, { wait: true }).then(() => {

      }).catch(() => {
        
      });

      expect(this.collection.saving).toBeTruthy();

      // Fast-forward until all timers have been executed
      jest.runOnlyPendingTimers();

      expect(this.collection.saving).toBeFalsy();
    });
  });
});