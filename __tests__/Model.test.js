import omit from 'lodash.omit';
import request from 'axios';
import Model from '../src/Model';
import Collection from '../src/Collection';
import { user, company } from '../fixtures/models';
import { observable, computed, toJS } from 'mobx';

describe('Model', function() {
  describe('constructor with no initial state', function() {
    beforeEach(function() {
      spyOn(Model.prototype, 'set');

      this.collection = {};
      this.rootStore = {};

      this.model = new Model(null, {
        collection: this.store,
        rootStore: this.rootStore
      });
    });

    it ('Sets up a reference to the collection', function() {
      expect(this.model.collection).toEqual(this.store);
    });

    it ('Sets up a reference to the rootStore', function() {
      expect(this.model.rootStore).toEqual(this.rootStore);
    });

    it ('Sets up an internal uuid', function() {
      expect(this.model.uuid).toBeDefined();
    });

    it ('Does not call set method as there is no initial data', function() {
      expect(this.model.set).not.toHaveBeenCalled();
    });
  });

  describe('constructor with initial state', function() {
    beforeAll(function() {
      spyOn(Model.prototype, 'set');
    });

    beforeEach(function() {
      this.store = {};
      this.model = new Model({
        name: 'Nick',
        number: 1
      });
    });

    it ('Calls set method with the initial state', function() {
      expect(this.model.set).toHaveBeenCalledWith({
        name: 'Nick',
        number: 1
      }, { parse: true, stripUndefined: true, stripNonRest: true });
    });
  });

  describe('url method', function() {
    it('Returns url for the model.', function() {
      class SubModel extends Model {
        url() {
          return '/api/v1/user';
        }
      };

      this.model = new SubModel();

      expect(this.model.url()).toEqual('/api/v1/user');
    });

    it('can use the urlRoot property as a base.', function() {
      class SubModel extends Model {
        urlRoot = '/api/v1/people';
      };

      this.model = new SubModel();

      expect(this.model.url()).toEqual('/api/v1/people');

      this.model.id = 2;

      expect(this.model.id).toBe(2);
      expect(this.model.url()).toEqual('/api/v1/people/2');
    });

    it('can use the collection URL or function.', function() {
      this.collection = {
        url() {
          return '/api/v1/people';
        }
      };

      this.model = new Model(null, {
        collection: this.collection
      });

      expect(this.model.url()).toEqual('/api/v1/people');

      this.model.id = 2;

      expect(this.model.url()).toEqual('/api/v1/people/2');
    });
  });

  describe('uniqueId getter', function() {
    it('Returns the model.id property if it exists', function() {
      this.model = new Model({
        id: '5',
        name: 'nick'
      });

      expect(this.model.uniqueId).toEqual('5');
    });

    it('Returns the uuid property if the model is new', function() {
      const model = new Model();

      expect(model.uniqueId).toEqual(model.uuid);
    });
  });

  describe('request state properties', function() {
    beforeEach(function() {
      this.model = new Model();
    });

    it('Provides observable properties for models state', function() {
      expect(this.model.fetching).toBeDefined();
      expect(this.model.saving).toBeDefined();
    });
  });

  describe('setRequestLabel action', function() {
    beforeEach(function() {
      this.model = new Model();
    });

    it('Sets the give prop to the boolean value', function() {
      this.model.setRequestLabel('saving', true);
      expect(this.model.saving).toBeTruthy();
      this.model.setRequestLabel('saving', false);
      expect(this.model.saving).toBeFalsy();
    });
  });

  describe('set action', function() {
    beforeEach(function() {
      class SubModel extends Model {
        get restAttributes() {
          return ['id', 'name', 'phone'];
        };
      };

      this.model = new SubModel();
    });

    it('Runs the data through the parse method', function() {
      spyOn(Model.prototype, 'parse'); 

      this.model.set({
        name: 'Nick',
        phone: '021552497',
        address: '59 Rua Road'
      }, { parse: true });

      expect(Model.prototype.parse).toHaveBeenCalledWith({
        name: 'Nick',
        phone: '021552497',
        address: '59 Rua Road'
      });
    });

    it('Sets the passed in values as observables', function() {
      this.model.set({
        name: 'Nick',
        phone: '021552497',
        address: '59 Rua Road'
      });

      expect(this.model.name).toEqual('Nick');
      expect(this.model.phone).toEqual('021552497');
    });


    it('Strips out attributes that are not rest', function() {
      this.model.set({
        name: 'Nick',
        phone: '021552497',
        address: '59 Rua Road'
      });

      expect(this.model.name).toEqual('Nick');
      expect(this.model.phone).toEqual('021552497');
      expect(this.model.address).not.toBeDefined();
    });

    it('Strips out attributes that are undefined', function() {
      this.model.set({
        name: 'Nick',
        phone: '021552497',
        address: undefined
      });

      expect(this.model.name).toEqual('Nick');
      expect(this.model.phone).toEqual('021552497');
      expect(this.model.pick(['address'])).not.toBe({});
    });
  });

  describe('clear action', function() {
    beforeEach(function() {
      class SubModel extends Model {
        get restAttributes() {
          return ['id', 'name', 'phone'];
        };
      };

      this.model = new SubModel();
    });

    it('Clears all attributes', function() {
      this.model.set({
        id: '1',
        name: 'Nick',
        phone: '021552497'
      });

      expect(this.model.id).toEqual('1');
      expect(this.model.name).toBe('Nick');
      expect(this.model.phone).toBe('021552497');

      this.model.clear();

      expect(this.model.id).not.toBeDefined();
      expect(this.model.name).not.toBeDefined();
      expect(this.model.phone).not.toBeDefined();
    });
  });

  describe('fetch action', function() {
    beforeEach(function() {
      class SubModel extends Model {
        get restAttributes() {
          return [
            'id',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };

        url() {
          return '/api/v1/me';
        }
      };

      this.model = new SubModel(user);
    });

    it('Sets the "fetching" request label to truthy', function() {
      this.model.fetch().then(() => {

      }).catch(() => {

      });

      expect(this.model.fetching).toBeTruthy();
    });

    it('Calls a get request with the models url by default', function() {
      spyOn(request, 'get').and.callFake(function(url) {
        return {
          then: function(cb) {
            cb.call(null, {
              status: 200,
              data: user
            });

            return this;
          },
          catch: ()=> {}
         }
      });

      this.model.fetch().then(() => {

      }).catch(() => {
        
      });

      expect(request.get).toHaveBeenCalledWith(this.model.url(), { params: {} });
    });

    it('Calls a get request with the url passed in though options', function() {
      spyOn(request, 'get').and.callThrough();

      this.model.fetch({
        url: '/api/users/1'
      }).then(() => {

      }).catch(() => {
        
      });

      expect(request.get).toHaveBeenCalledWith('/api/users/1', { params: {} });
    });

    it('Sends the any "params" included in the options argument', function() {
      spyOn(request, 'get').and.callThrough();

      this.model.fetch({
        params: {
          included: 'companyes'
        }
      }).then(() => {

      }).catch(() => {
        
      });

      expect(request.get).toHaveBeenCalledWith(this.model.url(), { params: { included: 'companyes'} });
    });

    it('Calls the set method if the request is successful', function() {
      spyOn(Model.prototype, 'set');

      spyOn(request, 'get').and.callFake(function(url) {
        return {
          then: function(cb) {
            cb.call(null, {
              status: 200,
              data: user
            });

            return this;
          },
          catch: ()=> {}
         }
      });


      this.model.fetch().then(() => {

      }).catch(() => {
        
      });

      expect(this.model.set).toHaveBeenCalledWith(user);
    });

    it('Sets the "fetching" request label to falsy if the request fails', function() {
      spyOn(Model.prototype, 'set');

      spyOn(request, 'get').and.callFake(function(url) {
        return {
          then: function(cb) {
            return {
              catch: function(cb) {
                cb.call(null, {
                  status: 404
                });

                return this;
              }
            }
          }
         }
      });

      this.model.fetch().then(() => {

      }).catch(() => {
        
      });

      expect(this.model.set).not.toHaveBeenCalled();

      expect(this.model.fetching).toBeFalsy();
    });
  });

  describe('save action', function() {
    beforeEach(function() {
      class SubModel extends Model {
        get restAttributes() {
          return [
            'id',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };

        url() {
          return '/api/v1/me';
        }
      };

      this.model = new SubModel(user);
    });

    it('Delegates to the create method if the model is new', function() {
      class SubModel extends Model {
        get restAttributes() {
          return [
            'id',
            'type',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };

        url() {
          return '/api/v1/me';
        }
      };

      spyOn(Model.prototype, 'create');

      this.model = new SubModel();

      this.model.save(user, { wait: true });

      expect(Model.prototype.create).toHaveBeenCalledWith(user, { wait: true, stripNonRest: true });
    });

    it('Sends patch request to the URL if model has an ID', function() {
      this.patch = spyOn(request, 'patch').and.callThrough();

      this.model.save().then(() => {

      }).catch(() => {
        
      });

      expect(this.patch.calls.mostRecent().args[0]).toEqual(this.model.url());
    });

    it('Sends all attributes if data argument is missing', function() {
      this.patch = spyOn(request, 'patch').and.callThrough();
      this.model.save().then(() => {

      }).catch(() => {
        
      });

      expect(this.patch.calls.mostRecent().args[1]).toEqual({
        "id": "1",
        "title": "Mr",
        "firstName": "Nick",
        "lastName": "Ryall",
        "email": "nick.ryall@gmail.com",
        "phone": "021552497"
      });
    });

    it('Sends only the attributes in the data argument', function() {
      this.patch = spyOn(request, 'patch').and.callThrough();

      this.model.save({
        firstName: 'David'
      }).then(() => {

      }).catch(() => {
        
      });

      expect(this.patch.calls.mostRecent().args[1]).toEqual({
        firstName: 'David'
      });
    });

    it('Strips out any attributes not flagged in te restAttributes gettera', function() {
      this.patch = spyOn(request, 'patch').and.callThrough();

      this.model.save({
        firstName: 'David',
        age: 26
      }).then(() => {

      }).catch(() => {
        
      });

      expect(this.patch.calls.mostRecent().args[1]).toEqual({
        firstName: 'David'
      });
    });

    it('Immediately updates the attributes on the model if the "wait" option is falsey', function() {
      this.model.save({
        firstName: 'David'
      }, {
        wait: false
      }).then(() => {

      }).catch(() => {
        
      });

      expect(this.model.firstName).toEqual('David');
    });

    it('Does not Immediately update the attributes on the model if the "wait" option is truthy', function() {
      this.model.save({
        attributes: {
          firstName: 'Rick'
        }
      }, {
        wait: true
      }).then(() => {

      }).catch(() => {
        
      });

      expect(this.model.firstName).toEqual('Nick');
    });

    it('Sets the "saving" request label to truthy if the  "wait" option is truthy', function() {
      this.model.save({
        firstName: 'Tim'
      }, {
        wait: true
      }).then(() => {

      }).catch(() => {
        
      });

      expect(this.model.saving).toBeTruthy();
    });

    it('Calls the set method if the request is successful', function() {
      spyOn(Model.prototype, 'set');

      spyOn(request, 'patch').and.callFake(function(url) {
        return {
          then: function(cb) {
            cb.call(null, {
              status: 200,
              data: user
            });

            return this;
          },
          catch: ()=> {}
         }
      });

      this.model.save().then(() => {

      }).catch(() => {
        
      });

      expect(this.model.set).toHaveBeenCalledWith(user);
    });

    it('Resets the attributes to the original state if the request fails and wait option is falsy', function() {
      spyOn(Model.prototype, 'set');

      spyOn(request, 'patch').and.callFake(function(url) {
        return {
          then: function(cb) {
            return {
              catch: function(cb) {
                cb.call(null, {
                  status: 404
                });

                return this;
              }
            }
          }
         }
      });

      this.model.save({
        firstName: 'John'
      }, { wait: false }).then(() => {

      }).catch(() => {
        
      });

      expect(this.model.firstName).toEqual('Nick');
    });
  });

  describe('create action', function() {
    beforeEach(function() {
      class SubModel extends Model {
        get restAttributes() {
          return [
            'id',
            'type',
            'title',
            'firstName',
            'lastName',
            'email',
            'phone'
          ];
        };

        url() {
          return '/api/v1/user';
        }
      };

      this.model = new SubModel(omit(user, 'id'));
    });

    it('Posts all existing data if no new data is passed in', function() {
      this.post = spyOn(request, 'post').and.callThrough();
      this.model.create().then(() => {

      }).catch(() => {
        
      });

      expect(this.post.calls.mostRecent().args[1]).toEqual(omit(user, 'id'));
    });

    it('Merges in passed in data with any existing data before posting', function() {
      this.post = spyOn(request, 'post').and.callThrough();
      this.model.create({
        "phone": "0211912340"
      }).then(() => {

      }).catch(() => {
        
      });

      expect(this.post.calls.mostRecent().args[1]).toEqual({
        "type": "User",
        "title": "Mr",
        "firstName": "Nick",
        "lastName": "Ryall",
        "email": "nick.ryall@gmail.com",
        "phone": "0211912340"
      });
    });

    it('Immediately sets the data on the model if "wait" option is falsy', function() {
      spyOn(request, 'post').and.callFake(function(url) {
        return {
          then: function(cb) {
            setTimeout(() => {
              cb.call(null, {
                status: 201,
                  data: user
              });
            }, 1000);

            return this;
          },
          catch: ()=> {}
        }
      });

      this.model.create({
        "title": "Mr",
        "firstName": "Johnny",
        "lastName": "Ryall",
        "email": "nick.ryall@gmail.com",
        "phone": "0211912340"
      }, { wait: false }).then(() => {

      }).catch(() => {
        
      });

      expect(this.model.firstName).toEqual('Johnny');
    });

    it('Waits for successful response from server before updating model if "wait" option is truthy', function() {
      spyOn(request, 'post').and.callFake(function(url) {
        return {
          then: function(cb) {
            setTimeout(() => {
              cb.call(null, {
                status: 201,
                data: {
                  "title": "Mr",
                  "firstName": "Mike",
                  "lastName": "Ryall",
                  "email": "nick.ryall@gmail.com",
                  "phone": "0211912340"
                }
              });
            }, 1000);

            return this;
          },
          catch: ()=> {}
        }
      });

      this.model.create({
        "title": "Mr",
        "firstName": "Mike",
        "lastName": "Ryall",
        "email": "nick.ryall@gmail.com",
        "phone": "0211912340"
      }, { wait: true }).then(() => {

      }).catch(() => {
        
      });

      expect(this.model.firstName).toEqual('Nick');

      // Fast-forward until all timers have been executed
      jest.runAllTimers();


      expect(this.model.firstName).toEqual('Mike');
    });

    it('Sets the saving request label to truthy if "wait" option is truthy', function() {
      this.model.create({
        "title": "Mr",
        "firstName": "Timmy",
        "lastName": "Ryall",
        "email": "nick.ryall@gmail.com",
        "phone": "0211912340"
      }, { wait: true }).then(() => {

      }).catch(() => {
        
      });

      expect(this.model.saving).toBeTruthy();
    });

    it('sends the post request to the model url', function() {
      this.post = spyOn(request, 'post').and.callThrough();
      this.model.create().then(() => {

      }).catch(() => {
        
      });

      expect(this.post.calls.mostRecent().args[0]).toEqual(this.model.url());
    });

    it('sends the post request to the url passed in as "url" option', function() {
      this.post = spyOn(request, 'post').and.callThrough();
      this.model.create(null, {
        url: '/api/v1/people/1'
      }).then(() => {

      }).catch(() => {
        
      });

      expect(this.post.calls.mostRecent().args[0]).toEqual('/api/v1/people/1');
    });

    it('calls the models set action with the response from successful save to server', function() {
      spyOn(Model.prototype, 'set');

      spyOn(request, 'post').and.callFake(function(url) {
        return {
          then: function(cb) {
            cb.call(null, {
              status: 201,
              data: user
            });

            return this;
          },
          catch: ()=> {}
         }
      });

      this.model.create().then(() => {

      }).catch(() => {
        
      });

      expect(this.model.set).toHaveBeenCalledWith(user);
    });

    it('resets to the original attributes on failed save to server if "wait" option is falsy', function() {
      spyOn(Model.prototype, 'set');

      spyOn(request, 'post').and.callFake(function(url) {
        return {
          then: function(cb) {
            return {
              catch: function(cb) {
                cb.call(null, {
                  status: 404
                });

                return this;
              }
            }
          }
         }
      });

      this.model.create({
        firstName: 'John'
      }, { wait: false }).then(() => {

      }).catch(() => {
        
      });

      expect(this.model.firstName).toEqual('Nick');
    });
  });

  describe('destroy action', function() {
    beforeEach(function() {
      class SubCollection extends Collection {
        url() {
          return '/api/v1/companyes';
        }
      };

      const newModel = new Model();

      newModel.set({
        name: 'New company'
      });

      this.collection = new SubCollection();
      this.collection.add([
        company,
        newModel
      ]);
    });

    it('should immediately remove the model from the parent collection if the model is new', function() {
      this.collection.at(1).destroy();
      expect(this.collection.length).toEqual(1);
    });

    it('should immediately remove the model from the parent collection if the "wait" option is falsy', function() {
      this.collection.at(0).destroy({ wait: false }).then(() => {

      }).catch((error) => {
        
      });;

      expect(this.collection.length).toEqual(1);
    });

    it('should put the models back in the parent collection if the request fails', function() {
      spyOn(request, 'delete').and.callFake(function(url) {
        return {
          then: function(cb) {
            return {
              catch: function(cb) {
                cb.call(null, {
                  status: 500
                });

                return this;
              }
            }
          }
         }
      });

      this.collection.at(0).destroy({ wait: false }).then(() => {

      }).catch((error) => {
        
      });
      expect(this.collection.length).toEqual(2);
      expect(this.collection.at(0).deleting).toBeFalsy();
    });

    it('Sends delete request to the URL of the model', function() {
      const deleteRequest = spyOn(request, 'delete').and.callThrough();

      this.collection.at(0).destroy().then(() => {

      }).catch(() => {
        
      });

      expect(deleteRequest).toHaveBeenCalledWith(`${this.collection.url()}/1`);
    });

    it('Sets the "deleting" requestLabel to truthy if "wait" option if truthy', function() {
      this.collection.at(0).destroy({ wait: true }).then(() => {

      }).catch(() => {
        
      });;
      expect(this.collection.at(0).deleting).toBeTruthy();
    });

    it('Waits until a successful response from server before removing model from collection if "wait" options is truthy', function() {
      spyOn(request, 'delete').and.callFake(function(url) {
        return {
          then: function(cb) {
            setTimeout(() => {
              cb.call(null, {
                status: 200
              });
            }, 1000);

            return this;
          },
          catch: () => {}
        }
      });

      this.collection.at(0).destroy({ wait: true }).then(() => {

      }).catch(() => {
        
      });

      expect(this.collection.length).toEqual(2);
      expect(this.collection.at(0).deleting).toBeTruthy();

      // Fast-forward until all timers have been executed
      jest.runAllTimers();

      expect(this.collection.length).toEqual(1);
      expect(this.collection.at(0).deleting).toBeFalsy();
    });
  });
});