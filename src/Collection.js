import _camelCase from 'lodash.camelcase';
import _last from 'lodash.last';
import _isEmpty from 'lodash.isempty';
import _difference from 'lodash.difference';
import { observable, asMap,  action, computed, runInAction } from 'mobx';
import request from 'axios';

import Model from './Model';

class Collection {
  @observable fetching;
  @observable saving;

  constructor(data, options = {}) {
    this.paging = observable.map({});
    this.models = observable([]);
    
    this.fetching = false;
    this.saving = false;

    if (options.rootStore) {
      this.rootStore = options.rootStore;
    }
    
    if (!_isEmpty(data)) {
      this.set(data);
    }
  }

  /**
   * The collection URL
   */
  url() {
    return '/';
  }

  /**
   * Specifies the model class for that collection
   */
  model() {
    return Model;
  }

  /**
   * Gets the unique ids of all the items in the collection
   */
  ids() {
    return this.models.map((model) => model.uniqueId);
  }

  /**
   * Getter for the collection length
   */
  @computed get length() {
    return this.models.length;
  }

  /**
   * Get a model at a given position
   */
  at(index) {
    return this.models[index];
  }

  /**
   * Get a model with the given id or uuid
   */
  get(uniqueId) {
    return this.models.find((model) => model.uniqueId === uniqueId);
  }

  /**
   * Controls boolean value of request label
   */
  @action setRequestLabel(label, state = false) {
    this[label] = state;
  }

  /**
   * Handles full JSON payload and sets data accordingly.
   */
  @action set(data, options) {
    // Merge in the any options with the default
    options = Object.assign({ 
      add: true, 
      merge: true, 
      remove: true 
    }, options);

    if (data.page) {
      this.setPaging(data.page);
    }

    if (data.data) {
      this.setModels(data.data, options);
    }
  }

  /**
   * Sets the links data into the collection.
   */
  @action setPaging(paging = {}) {
    this.paging.merge(paging);
  }

  /**
   * Get a link with the given key
   */
  getPagingKey(key) {
    return this.paging.get(key);
  }

  /**
   * Shortcut to the next page
   */
  @computed get nextPage() {
    return this.getPagingKey('next');
  }

  /**
   * Shortcut to the previous page
   */
  @computed get previousPage() {
    return this.getPagingKey('previous');
  }

  /**
   * Sets the models into the collection.
   *
   * You can disable adding, merging or removing.
   */
  @action setModels(models = [], options) {
    // Merge in the any options with the default
    options = Object.assign({ 
      add: true, 
      merge: true, 
      remove: true 
    }, options );

    if (options.remove) {
      const ids = models.map((d) => d.id);

      this.spliceModels(_difference(this.ids(), ids));
    }

    models.forEach((data) => {
      let model = this.get(data.id);

      if (model && options.merge) model.set(data);

      if (!model && options.add) this.pushModels(data);
    })
  }
  
  /**
   * Add a model (or an array of models) to the collection
   */
  @action add(models) {
    // Handle single model
    if (!Array.isArray(models)) models = [models];

    this.setModels(models, { add: true, merge: false, remove: false })

    return this.models;
  }

  /**
   * Adds a collection of models.
   * Returns the added models.
   */
  @action pushModels(models) {
    // Handle single model
    if (!Array.isArray(models)) models = [models];

    // Get the type of Model
    const CollectionModel = this.model();

    const instances = models.map((model) => {
      if (model instanceof Model) {
        if (!(model instanceof CollectionModel)) {
          throw new Error(`Collection can only hold ${CollectionModel.name} models.`);
        } else {
          if (!model.collection) {
            model.collection = this;
          }

          if (!model.rootStore && this.rootStore) {
            model.rootStore = this.rootStore;
          }

          return model;
        }
      } else {
        return new CollectionModel(model, {
          collection: this,
          rootStore: this.rootStore
        });
      }
    });

    this.models.push(...instances);

    return instances;
  }

  /**
   * Remove a model (or an array of models) from the collection
   */
  @action remove(models) {
    // Handle single model
    if (!Array.isArray(models)) models = [models];

    const ids = models.map((model) => {
      return model.uniqueId;
    });

    this.spliceModels(ids);
  }

  /**
   * Removes the models with the given ids or uuids
   */
  @action spliceModels(ids = []) {
    ids.forEach((id) => {
      const model = this.get(id);
      if (!model) return;

      this.models.splice(this.models.indexOf(model), 1);
    })
  }

  /**
   * Fetches the collection data from the backend.
   *
   * It uses `set` internally so you can
   * use the options to disable adding, changing
   * or removing.
   */
  @action fetch(options = {}) {
    // Merge in the any options with the default
    options = Object.assign({ 
      url: this.url(),
      params: {},
      add: true, 
      merge: true, 
      remove: true 
    }, options );

    this.setRequestLabel('fetching', true);

    return new Promise((resolve, reject) => {
      // Optionally the request above could also be done as
      request.get(options.url, {
        params: options.params
      })
      .then((response) => {
        runInAction('fetch-success', () => {
          this.set(response.data, { add: options.add, merge: options.merge, remove: options.remove });
          this.setRequestLabel('fetching', false);
          resolve(this, response);
        });
      })
      .catch((error) => {
        runInAction('fetch-error', () => {
          this.setRequestLabel('fetching', false);
          reject(error);
        });
      });
    });
  }

  /**
   * Creates the model and saves it on the backend
   *
   * The default behaviour is optimistic but this
   * can be tuned.
   */
  @action create(data = {}, options = { wait: false }) {
    // Don't add/create existing models
    if (data.id && this.get(data.id)) return false;

    const ModelClass = this.model();

    const model = new ModelClass(data);

    return new Promise((resolve, reject) => {
      if (!options.wait) {
        this.add(model);
        resolve(model);
      } else {
        this.setRequestLabel('saving', true);
      }

      // Model can create itself
      model.create(
        null,
        {
          url: this.url()
        }
      )
      .then((model, response) => {
        runInAction('create-success', () => {
          if (options.wait) {
            this.add(model);
          }

          this.setRequestLabel('saving', false);

          resolve(model, response);
        });
      })
      .catch((error) => {
        runInAction('create-error', () => {
          this.setRequestLabel('saving', false);

          // Remove the model if unsuccessful
          this.remove(model);

          reject(error);
        });
      });
    });
  }
}

export default Collection;